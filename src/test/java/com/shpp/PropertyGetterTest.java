package com.shpp;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;

class PropertyGetterTest {
    private static final String SEPARATOR = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyGetterTest.class);

    @Test
    void getPropertiesExceptionTest() {
        PropertyGetter.getProperties("noName.file");
        String expected = "Error: file properties was not found...";
        String received = PropertyGetter.excMessage;

        assertTrue(received.contains(expected));
        LOGGER.info(SEPARATOR);
        LOGGER.info("getPropertiesExceptionTest passed successfully...");
    }
}
