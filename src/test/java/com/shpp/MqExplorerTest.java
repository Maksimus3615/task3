package com.shpp;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MqExplorerTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MqExplorerTest.class);
    private static final String SEPARATOR = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
    @Test
    void mainTestValidLimitOfMessages() {
        String[] args = new String[1];
        args[0] = "10";
        MqExplorer.main(args);
        List<String> sentList = MqSender.messages;
        List<String> receivedList = MqReceiver.messages;

        assertEquals(sentList, receivedList);
        LOGGER.info(SEPARATOR);
        LOGGER.info("mainTestValidLimitOfMessages passed successfully...");
    }

    @Test
    void mainTestInvalidLimitOfMessages() {
        String[] args = new String[1];
        args[0] = "a10";
        MqExplorer.main(args);
        List<String> sentList = MqSender.messages;
        List<String> receivedList = MqReceiver.messages;

        assertEquals(sentList, receivedList);
        LOGGER.info(SEPARATOR);
        LOGGER.info("mainTestInvalidLimitOfMessages passed successfully...");
    }
}