package com.shpp;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

public class Pojo {
    @Size(min = 7, message = "name must contain at least 7 letters")
    @Pattern(regexp = "[\\w]*[Aa]+[\\w]*", message = "name must contain at least one letter 'A' or 'a'")
    @NotNull(message = "name cannot be null")
    private String name;
    @NotNull(message = "count cannot be null")
    @Min(value = 10, message = "count must be greater than 10 (ten)")
    private int count;
    @NotNull(message = "date cannot be null")
    private String date;

    public Pojo(String name, int count, String date) {
        this.name = name;
        this.count = count;
        this.date = date;
    }

    public Pojo() {
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public String getDate() {
        return date;
    }

}
