package com.shpp;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import javax.jms.*;
import javax.jms.IllegalStateException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class MqReceiver {
    private MqReceiver() throws IllegalStateException {
        throw new IllegalStateException("Utility class");
    }

    static List<String> messages;
    static int valid;
    static int invalid;
    private static final Logger LOGGER = LoggerFactory.getLogger(MqReceiver.class);
    //private static final String SEPARATOR = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
    private static final String PROPERTIES_PATH = "mq.properties";
    private static final String VALID_FILE_NAME = "valid.csv";
    private static final String INVALID_FILE_NAME = "invalid.csv";

    public static void receiveMessage() throws JMSException {
        messages = new ArrayList<>();
        Properties property = PropertyGetter.getProperties(PROPERTIES_PATH);

        LOGGER.info("A connection to ActiveMQ is being created...");
        final ActiveMQConnectionFactory connectionFactory =
                Connector.createActiveMQConnectionFactory();
        final PooledConnectionFactory pooledConnectionFactory =
                Connector.createPooledConnectionFactory(connectionFactory);
        final Connection consumerConnection = connectionFactory.createConnection();
        consumerConnection.start();
        LOGGER.info("A session is being created...");
        final Session consumerSession = consumerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);
        LOGGER.info("A queue is being created...");
        final Destination consumerDestination = consumerSession
                .createQueue(property.getProperty("mqName"));
        LOGGER.info("A message consumer is being created...");
        final MessageConsumer consumer = consumerSession
                .createConsumer(consumerDestination);
        // LOGGER.info(SEPARATOR);

        createCsvFiles();
        LOGGER.info("Created two files for data distribution...");

        distributeData(consumer);

        consumer.close();
        consumerSession.close();
        consumerConnection.close();
        pooledConnectionFactory.stop();
    }

    private static void distributeData(MessageConsumer consumer) {
        try (FileWriter validWriter = new FileWriter(VALID_FILE_NAME, true);
             FileWriter invalidWriter = new FileWriter(INVALID_FILE_NAME, true)) {
            valid = 0;
            invalid = 0;
            int messageCounter = 0;
            ObjectMapper objectMapper = new ObjectMapper();
            ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
            Validator validator = validatorFactory.getValidator();

            LOGGER.info("Receiving messages...");

            while (true) {
                final Message consumerMessage = consumer.receive(1000);
                if (consumerMessage != null) {

                    final TextMessage consumerTextMessage = (TextMessage) consumerMessage;
                    String json = consumerTextMessage.getText();
                    // String mess = "Message received: " + json;
                    // LOGGER.info(mess);
                    messageCounter++;
                    writeCsvFiles(json, validator, objectMapper, validWriter, invalidWriter);
                } else {
                    // LOGGER.info(SEPARATOR);
                    LOGGER.info("Queue is empty...");
                    String text = messageCounter + " messages were received: "+valid + " - valid, "+invalid+" - invalid.";
                    LOGGER.info(text);
                    break;
                }
            }
            validatorFactory.close();
            validWriter.flush();
            invalidWriter.flush();
        } catch (IOException | JMSException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
    }

    private static void writeCsvFiles(String json,
                                      Validator validator,
                                      ObjectMapper objectMapper,
                                      FileWriter validWriter,
                                      FileWriter invalidWriter) throws IOException {

        messages.add(json);
        Pojo pojo = objectMapper.readValue(json, Pojo.class);
        Set<ConstraintViolation<Pojo>> setOfErrors = validator.validate(pojo);

        if (setOfErrors.isEmpty()) {
            valid++;
            validWriter.write("" + pojo.getName() + "," + pojo.getCount() + "\n");
        } else {
            invalid++;
            Error errorObject = new Error(setOfErrors);
            String errorJsonFormat = objectMapper.writeValueAsString(errorObject);

            invalidWriter.write(pojo.getName() + "," + pojo.getCount() + ","
                    + pojo.getDate() + "," + errorJsonFormat + "\n");
        }
    }

    private static void createCsvFiles() {
        File validFile = new File(VALID_FILE_NAME);
        File invalidFile = new File(INVALID_FILE_NAME);
        try (FileWriter validWriter = new FileWriter(validFile, false);
             FileWriter invalidWriter = new FileWriter(invalidFile, false)) {
            validWriter.write("name,count\n");
            invalidWriter.write("name,count,date,errors\n");
            validWriter.flush();
            invalidWriter.flush();
        } catch (IOException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
    }
}
