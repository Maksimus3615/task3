package com.shpp;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import java.util.List;
import java.util.Properties;

public class Connector {
    private static final String PROPERTIES_PATH = "mq.properties";
    private static final String TRUSTED_PACKAGE = "com.shpp";
    private static final int MAX_CONNECTIONS = 10;

    private Connector() {
        throw new IllegalStateException("Utility class");
    }
    public static PooledConnectionFactory
    createPooledConnectionFactory(ActiveMQConnectionFactory connectionFactory) {
        final PooledConnectionFactory pooledConnectionFactory =
                new PooledConnectionFactory();
        pooledConnectionFactory.setConnectionFactory(connectionFactory);
        pooledConnectionFactory.setMaxConnections(MAX_CONNECTIONS);
        return pooledConnectionFactory;
    }

    public static ActiveMQConnectionFactory createActiveMQConnectionFactory() {
        Properties property = PropertyGetter.getProperties(PROPERTIES_PATH);
        final ActiveMQConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(property.getProperty("ssl"));
        connectionFactory.setTrustedPackages(List.of(TRUSTED_PACKAGE));
        connectionFactory.setUserName(property.getProperty("user"));
        connectionFactory.setPassword(property.getProperty("password"));
        return connectionFactory;
    }
}
