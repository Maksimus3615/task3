package com.shpp;

import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Random;

public class Randomizer {
    private static final Random random = new Random();
    private static final int COUNT_MIN = 0;
    private static final int COUNT_MAX = 1000;
    private static final int LENGTH_NAME_MIN = 5;
    private static final int LENGTH_NAME_MAX = 15;
    private static final int NOT_LETTER_SYMBOL_MIN = 91;
    private static final int NOT_LETTER_SYMBOL_MAX = 96;
    private static final int NUMBER_OF_LETTER_MIN = 65;
    private static final int NUMBER_OF_LETTER_MAX = 123;
    private static final String STOP_WORD = "poison_pill";
    private static final String PROPERTIES_PATH = "mq.properties";

    private Randomizer() {
        throw new IllegalStateException("Utility class");
    }
    public static String getDate() {
        long day = random.nextInt(365 * 10);
        long minute = random.nextInt(60 * 24);
        long nano = random.nextInt((int) (1e+10));
        return LocalDateTime.now().minusDays(day)
                .minusMinutes(minute)
                .minusNanos(nano)
                .toString();
    }

    public static int getCount() {
        return random.nextInt(COUNT_MAX - COUNT_MIN) + COUNT_MIN;
    }

    public static String getName(long startTime) {
        Properties property = PropertyGetter.getProperties(PROPERTIES_PATH);
        long stopTime = Integer.parseInt(property.getProperty("stopTime"))*1000L; //mls

        if ((System.currentTimeMillis() - startTime) < stopTime) {
            return returnRandomName(returnRandomLength());
        } else return STOP_WORD;
    }

    private static int returnRandomLength() {
        return random.nextInt(LENGTH_NAME_MAX - LENGTH_NAME_MIN) +
                LENGTH_NAME_MIN;
    }

    private static String returnRandomName(int length) {
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int charNum = random.nextInt(NUMBER_OF_LETTER_MAX - NUMBER_OF_LETTER_MIN) +
                    NUMBER_OF_LETTER_MIN;
            if (charNum < NOT_LETTER_SYMBOL_MIN || charNum > NOT_LETTER_SYMBOL_MAX) {
                name.append((char) (charNum));
            }
        }
        return name.toString();
    }
}
