package com.shpp;

import jakarta.validation.ConstraintViolation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Error {
    private final ArrayList<String> errors;

    public Error(Set<ConstraintViolation<Pojo>> setOfErrors) {
        this.errors = new ArrayList<>();
        for (ConstraintViolation<Pojo> error : setOfErrors) {
            this.errors.add(error.getMessage());
        }
    }

    public List<String> getErrors() {
        return errors;
    }
}
