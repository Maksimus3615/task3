package com.shpp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.jms.pool.PooledConnectionFactory;

import javax.jms.*;
import javax.jms.IllegalStateException;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

public class MqSender {

    private MqSender() throws IllegalStateException {
        throw new IllegalStateException("Utility class");
    }

    static List<String> messages;
    private static final Logger LOGGER = LoggerFactory.getLogger(MqSender.class);
    private static final String STOP_WORD = "poison_pill";
    private static final String PROPERTIES_PATH = "mq.properties";

    public static void sendMessage(int limitOfMessages) throws JMSException {
        messages = new ArrayList<>();
        Properties property = PropertyGetter.getProperties(PROPERTIES_PATH);
        LOGGER.info("A connection to ActiveMQ is being created...");
        final ActiveMQConnectionFactory connectionFactory =
                Connector.createActiveMQConnectionFactory();
        final PooledConnectionFactory pooledConnectionFactory =
                Connector.createPooledConnectionFactory(connectionFactory);
        final Connection producerConnection =
                pooledConnectionFactory.createConnection();
        producerConnection.start();
        LOGGER.info("A session is being created...");
        final Session producerSession = producerConnection
                .createSession(false, Session.AUTO_ACKNOWLEDGE);
        LOGGER.info("A queue is being created...");
        final Destination producerDestination = producerSession
                .createQueue(property.getProperty("mqName"));
        LOGGER.info("A message producer is being created...");
        final MessageProducer producer = producerSession
                .createProducer(producerDestination);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        LOGGER.info("Messages are being created and sent to the queue...");
        long startTime = System.currentTimeMillis();
        ObjectMapper objectMapper = new ObjectMapper();

        getDataStream(startTime, limitOfMessages)
                .forEach(i -> sendMessageJsonFormat(i, producer, producerSession, objectMapper));

        long finishTime = System.currentTimeMillis();
        LOGGER.info("Data transfer process completed...");
        String dataInform = messages.size() + " messages were sent...";
        LOGGER.info(dataInform);
        String timeInform = "The data transfer time is " + (finishTime - startTime + " mls");
        LOGGER.info(timeInform);

        producer.close();
        producerSession.close();
        producerConnection.close();
        pooledConnectionFactory.stop();
    }

    private static void sendMessageJsonFormat(Pojo pojo,
                                              MessageProducer producer,
                                              Session producerSession,
                                              ObjectMapper objectMapper) {
        try {
            producer.send(producerSession.createTextMessage(toJson(pojo, objectMapper)));
        } catch (JMSException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
    }

    private static Stream<Pojo> getDataStream(long startTime, int limitOfMessages) {
        return Stream.generate(() -> new Pojo(Randomizer.
                        getName(startTime),
                        Randomizer.getCount(),
                        Randomizer.getDate()))
                .takeWhile(x -> !x.getName().equals(STOP_WORD))
                .limit(limitOfMessages);
    }

    private static String toJson(Pojo pojo, ObjectMapper objectMapper) {
        String json = "";
        try {
            json = objectMapper.writeValueAsString(pojo);
            messages.add(json);
        } catch (JsonProcessingException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
        return json;
    }
}
