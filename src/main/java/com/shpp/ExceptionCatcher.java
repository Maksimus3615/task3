package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExceptionCatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionCatcher.class);

    private ExceptionCatcher() {
        throw new IllegalStateException("Utility class");
    }
    public static void makeStackTraceMessage(Exception e) {
        LOGGER.error(e.getMessage());
        StackTraceElement[] stack = e.getStackTrace();
        String errorText = stack[0].toString();
        LOGGER.error(errorText);
    }
}