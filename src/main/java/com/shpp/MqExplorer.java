package com.shpp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;

public class MqExplorer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MqExplorer.class);
    private static final String SEPARATOR = "************************************************************************";
    private static final String SEPARATOR_1 = "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!";
    private static final int DEFAULT_LIMIT_OF_MESSAGES = 10;

    public static void main(String[] args) {
        sendMessagesToActiveMq(getLimitOfMessagesFromArgs(args));
        receiveMessagesFromActiveMq();
        LOGGER.info("************************JOB IS DONE**********************");
        LOGGER.info("************************JOB IS DONE**********************");
        LOGGER.info("************************JOB IS DONE**********************");
        LOGGER.info("************************JOB IS DONE**********************");
        LOGGER.info("************************JOB IS DONE**********************");
    }

    private static void receiveMessagesFromActiveMq() {
        try {
            LOGGER.info(SEPARATOR);
            LOGGER.info("Starts receiving messages from the queue...");
            MqReceiver.receiveMessage();
            LOGGER.info("Receiving messages finished.");
        } catch (JMSException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
    }

    private static void sendMessagesToActiveMq(int limitOfMessages) {
        try {
            LOGGER.info(SEPARATOR);
            LOGGER.info("Starts sending messages to the queue...");
            MqSender.sendMessage(limitOfMessages);
            LOGGER.info("Sending messages finished.");
        } catch (JMSException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
        }
    }

    private static int getLimitOfMessagesFromArgs(String[] args) {
        int limitOfMessages = DEFAULT_LIMIT_OF_MESSAGES;
        if (args.length != 0) {
            try {
                limitOfMessages = Integer.parseInt(args[0]);
            } catch (Exception e) {
                LOGGER.warn(SEPARATOR_1);
                LOGGER.warn("Illegal format of limitOfMessages...");
                LOGGER.warn("Default limit set to " + DEFAULT_LIMIT_OF_MESSAGES + " messages...");
            }
        }
        return limitOfMessages;
    }
}