package com.shpp;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class PropertyGetter {
    static String excMessage;

    private PropertyGetter() {
        throw new IllegalStateException("Utility class");
    }

    public static Properties getProperties(String path) {
        excMessage = "No exception in the method getProperties()...";
        Properties properties = new Properties();
        InputStream in = MqExplorer.class.getClassLoader().getResourceAsStream(path);
        try {
            if (in != null) {
                InputStreamReader inReader = new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader buff = new BufferedReader(inReader);
                properties.load(buff);
            } else {
                throw new FileNotFoundException("Error: file properties was not found...");
            }
        } catch (IOException e) {
            ExceptionCatcher.makeStackTraceMessage(e);
            excMessage = e.getMessage();
        }
        return properties;
    }
}